import { createPortafolio } from "utils/portafolioCreator";
import gamedev from "./skills/gamedev";
import cleanCode from "./skills/cleanCode";
import teamWork from "./skills/teamWork";
import webdev from "./skills/webdev";
import languages from "./skills/languages";
import gote from "./projects/gote";
import wb from "./projects/wb";
import babel from "./projects/babel";
import ggj2021 from "./projects/ggj2021";
import age from "portafolios/age";
// import project2 from "./projects/project2";

export default createPortafolio({
  title: "Portafolio",
  text: `Soy José Guerra un desarrollador de videojuegos de Guatemala, tengo ${age} años y he desarrollado (y jugado) juegos desde que era un niño.
  
  Me gradué en el [Centro Universitario de Oriente](https://cunori.edu.gt/)   en  Ingeniería en Ciencias y Sistemas. Actualmente estoy buscando desarrollar juegos para grandes empresas y expandir mi conocimiento y experiencia en el área de desarrollo de videojuegos.


  Puedes ver mi CV [aquí](resume/CV-Jose-Guerra-Espanol.pdf).
  `,
  profilePictureSrc: "/images/avatars/me.png",
  email: "josguerra99@gmail.com",
  skills: [gamedev, webdev, teamWork, cleanCode],
  projects: [wb, gote, babel, ggj2021],
});
