import { createSkill, tag } from "utils/portafolioCreator";

const tags = [
  tag("Unity", "https://unity.com/"),
  tag("Playfab", "https://playfab.com/"),
  tag(
    "AR Core",
    "https://developers.google.com/ar/develop/unity/quickstart-android?hl=en"
  ),
  tag("Vuforia", "https://developer.vuforia.com/"),
  tag("Mirror Networking", "https://mirror-networking.com/"),
];

export default createSkill({
  title: "Desarrollo de videojuegos",
  text: `
  Tengo más de 8 años de experiencia en el desarrollo de videojuegos en Unity, en esos años he creado muchos proyectos personales que me han permitido ganar un conocimiento extenso de este software.

  Desde 2018 he completado dos juegos grandes, un **multijugador** competitivo para PC (con servidor dedicado) y un juego **móvil**. También he trabajado en proyectos con el uso de **Realidad Aumentada**.
  `,
  tags,
});
