import { createSkill, tag } from "utils/portafolioCreator";

const tags = [
  tag("NodeJS", "https://nodejs.org/en/"),
  tag("ReactJS", "https://reactjs.org/"),
  tag("NextJS", "https://nextjs.org/"),
  tag("MySQL", "https://www.mysql.com/"),
  tag("SQLite", "https://www.sqlite.org/index.html"),
  tag("Firebase", "https://firebase.google.com/"),
  tag("MongoDB", "https://www.mongodb.com/"),
  tag("ElasticSearch", "https://www.elastic.co/"),
  tag("Redis", "https://redis.io/"),
  tag("Digital Ocean", "https://www.digitalocean.com/"),
  tag("AWS", "https://aws.amazon.com/"),
];

export default createSkill({
  title: "Desarrollo Web (Frontend y Backend)",
  text: `
  He hecho una gran variedad de proyectos web, donde he creado páginas web y APIs, utilizando tecnologías como ReactJS, NextJS y NodeJS.   
  Además de eso puedo trabajar con bases de datos (SQL y NoSQL).`,
  tags,
});
