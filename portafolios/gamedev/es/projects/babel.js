import { createProject, media, tag } from "utils/portafolioCreator";

const tags = [
  tag("Unity", "https://unity.com/"),
  tag(
    "AR Core",
    "https://developers.google.com/ar/develop/unity/quickstart-android?hl=en"
  ),
  tag("Unity Collaborate", "https://unity.com/unity/features/collaborate"),
  tag("NodeJS", "https://nodejs.org/en/"),
  tag("ReactJS", "https://reactjs.org/"),
  tag("MySQL", "https://www.mysql.com/"),
];

const medias = [
  media("Highreel", "https://www.youtube.com/watch?v=JmsvzqqhNTE"),
];

export default createProject({
  title: "Babel AR 📱",
  text: `

Es una aplicación AR enfocada en enseñar conceptos matemáticos y físicos 
en una manera más digerible para los estudiantes, haciendo uso de tecnologías
3D y de realidad aumentada. Con esta tecnología podemos obtener una mejor visualización de conceptos
que serían difíciles de entender en una pizarra.`,
  tags,
  media: medias,
});

/**
 * 
Babel AR was created for a student project at my University, 
and it had a very different set of challenges, one of them is that AR is still in its infancy and
            hasn’t fully develop yet, I started develolping this project in 2018
            so AR was even rarer and the implementation of it was still very
            new.
 */
