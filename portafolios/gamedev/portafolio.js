import { createPortafolio } from "utils/portafolioCreator";
import gamedev from "./skills/gamedev";
import cleanCode from "./skills/cleanCode";
import teamWork from "./skills/teamWork";
import webdev from "./skills/webdev";
import languages from "./skills/languages";
import gote from "./projects/gote";
import wb from "./projects/wb";
import babel from "./projects/babel";
import ggj2021 from "./projects/ggj2021";
import age from "portafolios/age";
// import project2 from "./projects/project2";








export default createPortafolio({
  title: "Portfolio",
  text: `Hey, I’m José Guerra a game developer from Guatemala, I am ${age} years old and have been developing (and playing) games since i was a kid.
  
  I graduated from [Centro Universitario de Oriente](https://cunori.edu.gt/)   in science and systems engineering and I’m currently looking to develop games for
      great companies and expand my knowledge and experience about the game
      development industry.
  
  
  You can check my resume [here](resume/CV-Jose-Guerra-English.pdf).
      `,
  profilePictureSrc: "/images/avatars/me.png",
  email: "josguerra99@gmail.com",
  skills: [gamedev, webdev, teamWork, cleanCode],
  projects: [wb, gote, babel, ggj2021],
});
