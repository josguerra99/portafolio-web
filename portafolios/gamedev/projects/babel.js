import { createProject, media, tag } from "utils/portafolioCreator";

const tags = [
  tag("Unity", "https://unity.com/"),
  tag(
    "AR Core",
    "https://developers.google.com/ar/develop/unity/quickstart-android?hl=en"
  ),
  tag("Unity Collaborate", "https://unity.com/unity/features/collaborate"),
  tag("NodeJS", "https://nodejs.org/en/"),
  tag("ReactJS", "https://reactjs.org/"),
  tag("MySQL", "https://www.mysql.com/"),
];

const medias = [
  media("Highreel", "https://www.youtube.com/watch?v=JmsvzqqhNTE"),
];

export default createProject({
  title: "Babel AR 📱",
  text: `
 It's an AR application focused on teaching maths and physics concepts
            in a more digestable way to students, making full use not only from
            3D but Augmented Reality. With this technology we can get a better visualization of
            concepts that would be harder to understand on a board.`,
  tags,
  media: medias,
});

/**
 * 
Babel AR was created for a student project at my University, 
and it had a very different set of challenges, one of them is that AR is still in its infancy and
            hasn’t fully develop yet, I started develolping this project in 2018
            so AR was even rarer and the implementation of it was still very
            new.
 */
